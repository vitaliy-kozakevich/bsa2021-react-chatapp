import React, { useState, useEffect } from 'react'
import { ChatContext } from '../../components/context/Chat.context'
import { MessageContext } from '../../components/context/Message.context'
import Header from '../../components/Header/Header'
import { useHttp } from '../../components/hooks/http.hook'
import InputGroup from '../../components/InputGroup/InputGroup'
import MessageList from '../../components/MessageList/MessageList'
import { Wrapper } from '../../Layout/Wrapper'
import spinner from '../../assets/spinner.svg'

const humanReadableIdOffset = 100300200100

const user = {
    userId: new Date().getTime()- humanReadableIdOffset,
    user: 'Rain'
}

export default function Chat() {
    const [messages, setMessages] = useState([])
    const { loading, fetchMessages, error, clearError } = useHttp()
    const [usersCount, setUsersCounter] = useState([])
    const [messagesCount, setMessageCounter] = useState(0)
    const [inputValue, setInputValue] = useState('')
    const [lastMessageDate, setLastMessageDate] = useState(null)

    

    useEffect(async () => {
        try {
            const data = await fetchMessages()
            setMessageCounter(data.length)
            setMessages(data)
        } catch (error) {
            
        }
    }, [])

    useEffect(() => {
        const users = []
        messages.forEach(({ userId }) => {
            if (!users.includes(userId)) users.push(userId)
        })
        setUsersCounter(users.length)
    }, [messages])

    useEffect(() => {
        const length = messages.length
        if (length) {
            const lastMessageDate = messages[length - 1].createdAt
            setMessageCounter(length)
            setLastMessageDate(new Date(lastMessageDate).toDateString())
        }
    }, [messages])

    const inputValueHandler = (e) => {
        setInputValue(e.target.value)
    }

    const senMessageHandler = () => {
        if (!inputValue) return
        const newMessage = {
            ...user,
            id: new Date().getTime() - humanReadableIdOffset,
            text: inputValue,
            createdAt: new Date().toISOString(),
            editedAt: ''
        }
        const newMessages = [...messages]
        newMessages.push(newMessage)

        setMessages(newMessages)
        setInputValue('')
    }

    const editMessageHandler = (messageId, userId) => {

    }

    const deleteMessageHandler = (messageId, userId) => {
        const newMessages = [...messages]
        const filteredMessages = newMessages.filter(m => m.id !== messageId && m.userId !== userId)
        setMessages(filteredMessages)
    }

    return (
        <div>
            <Wrapper>
                <ChatContext.Provider value={{ inputValue, inputValueHandler, senMessageHandler, user }}>
                    { loading ?
                        <div style={{ textAlign: 'center', marginTop: '20%' }}>
                            <img src={'../../assets/spinner.svg'} />
                        </div> :
                        <>
                            <Header 
                                messagesCount={messagesCount} 
                                usersCount={usersCount}
                                lastMessageDate={lastMessageDate}
                            />
                            <MessageContext.Provider value={{ editMessageHandler, deleteMessageHandler }}>
                                <MessageList messages={messages}/> 
                            </MessageContext.Provider>
                            <InputGroup />
                        </> 
                    }
                </ChatContext.Provider>
            </Wrapper>
        </div>
    )
}
