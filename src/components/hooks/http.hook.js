import { useState, useCallback } from 'react';

export const useHttp = () => {
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(null);

    const fetchMessages = useCallback(
        async () => {
            setLoading(true)
            try {
                const response = await fetch('https://edikdolynskyi.github.io/react_sources/messages.json')
                const data = await response.json()

                if (!response.ok) {
                    throw new Error(data.message || 'Something went wrong!');
                }

                setLoading(false)

                return data
            } catch (error) {
                setLoading(false)
                throw error
            } 
        },
        [],
    )

    const clearError = useCallback(() => setError(null), []);

    return { loading, fetchMessages, error, clearError };
};
