import React, { useState, useEffect, Fragment } from 'react'
import Message from '../Message/Message'
import './MessageList.scss'

export default function MessageList({ messages }) {
    const [messageList, setMessageList] = useState(messages)

    useEffect(() => {
        setMessageList(messages)
    }, [messages])

    return (
        <div className='MessageList'>
            {
                messageList.map((m, index) => {
                    const { avatar, text, id, user, createdAt, userId } = m 
                    let isNewDateForMessage
                    if (index) {
                        isNewDateForMessage = new Date(messageList[index - 1].createdAt).getFullYear() > new Date(createdAt).getFullYear() ||
                                                new Date(messageList[index - 1].createdAt).getMonth() > new Date(createdAt).getMonth() ||
                                                new Date(messageList[index - 1].createdAt).getDay() > new Date(createdAt).getDay()
                    }

                    if (isNewDateForMessage) {
                        return (
                            <Fragment key={id}>
                                <div className='d-flex flex-end f-style'>
                                    <span>{new Date(createdAt).toDateString()}</span>
                                </div>
                                <hr />
                                <Message {...{ id, avatar, text, user, createdAt, userId }} key={id}/>
                            </Fragment>
                        )
                    }
                    return <Message {...{ id, avatar, text, user, createdAt, userId }} key={id}/>
                })
            }
        </div>
    )
}
