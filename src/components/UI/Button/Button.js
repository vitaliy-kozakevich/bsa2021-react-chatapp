import React from 'react'
import './Button.scss'

export default function Button({ clickHandler, name, cls }) {
    return (
        <button 
            className={cls? `Button ${cls}`: 'Button'}
            onClick={clickHandler}
        >
            { name }
        </button>
    )
}
