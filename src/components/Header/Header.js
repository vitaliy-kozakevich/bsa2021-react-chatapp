import React from 'react'
import './Header.scss'

export default function Header({ messagesCount, usersCount, lastMessageDate, chatName }) {
    return (
        <div className='Header'>
            <div className='Header_primary'>
                <span>{chatName}</span>
                <span>{messagesCount > 1 ? `${messagesCount} messages` : `${messagesCount} message`}</span>
                <span>{usersCount> 1 ? `${usersCount} users` : `${usersCount} user`}</span>
            </div>
            <div className='Header_secondary'>Last message at {lastMessageDate}</div>
        </div>
    )
}
