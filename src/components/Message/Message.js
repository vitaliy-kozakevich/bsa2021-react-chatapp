import React, { useContext } from 'react'
import { ChatContext } from '../context/Chat.context'
import { MessageContext } from '../context/Message.context'
import Button from '../UI/Button/Button'
import './Message.scss'

export default function Message({ id, avatar, text, user, createdAt: date, userId: user_id }) {
    const { user: { userId } } = useContext(ChatContext)
    const { editMessageHandler, deleteMessageHandler } = useContext(MessageContext)
    const parsedDate = new Date(date).toDateString()
    const isActiveUser = user_id === userId

    return (
        <div className={isActiveUser ? 'Message flex-end column' : 'Message'}>
            <div className='Message__wrap'>
                <div className='Message__info primary'>
                    <span className='Message__info-name'>{user}</span>
                    {
                        !isActiveUser ? 
                            <img className='Message__info-avatar' src={avatar} alt='user_avatar'/> :
                            null
                    }
                </div>
                <div className='Message__info secondary'>
                    <span className='Message__info-text'>{text}</span>
                    <span className='Message__info-date'>{parsedDate}</span>
                </div>
                
            </div>
            {
                !isActiveUser ? (
                    <div style={{ minWidth: '20px'}}>
                        {/* <img src={'../../assets/like.svg'} style={{ backgroundColor: 'lightblue' }}/> */}
                    </div>
                ) : null       
            }
            {
                isActiveUser ? (
                    <div className='Message__btns'>
                        <div className='Message__btns__btn'>
                            <Button clickHandler={deleteMessageHandler.bind(null, id, user_id)} name={'Delete'} cls={'error'} />
                        </div>
                        <div className='Message__btns__btn'>
                            <Button clickHandler={editMessageHandler.bind(null, id, user_id)} name={'Edit'} cls={'warn'} />
                        </div>
                    </div>
                ) : null    
            }
        </div>
    )
}
