import { createContext } from 'react'

const noop = () => {}

export const MessageContext = createContext({
    editMessageHandler: noop,
    deleteMessageHandler: noop,
})